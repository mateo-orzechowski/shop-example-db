<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\CallbackValidator;
use Symfony\Component\Validator\Context\ExecutionContext;

class OrderType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client', AutocompleteType::class, ['handler' => 'clientRepository.getAutocompleteOptions', 'class' => Client::class])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Pending' => 'pending',
                    'Processing' => 'processing',
                    'Complete' => 'complete',
                ]
            ])
            ->add('orderItems', CollectionType::class, [
                'entry_type' => OrderItemType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
           dump($event);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'constraints' => [
                new Callback([
                    'callback' => [$this, 'validateStatus']
                ])
            ]
        ]);
    }

    public function validateStatus(Order $value, ExecutionContext $context)
    {
        $oldValue = $this->entityManager->getUnitOfWork()->getOriginalEntityData($value);

        if ($oldValue['status'] !== 'pending') {
            return;
        }

        if ($value->getStatus() === 'pending') {
            return;
        }

        if($value->containsDeletedProducts()) {
            $context->buildViolation('Order cannot contain deleted products');
        }
    }
}
