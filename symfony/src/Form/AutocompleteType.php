<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class AutocompleteType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['handler'] = $options['handler'];
    }

    public function getBlockPrefix()
    {
        return 'app_autocomplete';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['handler']);
        $resolver->setAllowedTypes('handler', ['string']);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}
