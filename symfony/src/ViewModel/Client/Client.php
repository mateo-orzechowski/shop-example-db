<?php

declare(strict_types=1);

namespace App\ViewModel\Client;

class Client
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var string
     */
    private $email;

    public function __construct(int $id, string $name, string $surname, string $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getLabel(): string
    {
        $label = "{{ name }} {{ surname }} ({{ email }})";

        $label = str_replace('{{ name }}', $this->name, $label);
        $label = str_replace('{{ surname }}', $this->surname, $label);
        $label = str_replace('{{ email }}', $this->email, $label);

        return $label;
    }
}