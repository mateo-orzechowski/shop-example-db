<?php

declare(strict_types=1);

namespace App\ViewModel\Order;

use App\Entity\OrderItem;
use App\ViewModel\Client\Client;

class OrderShow
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var OrderItem[]
     */
    private $orderItems;

    /**
     * @var string
     */
    private $status;

    public function __construct(int $id, string $status, \DateTime $createdAt, Client $client, array $orderItems)
    {
        $this->id = $id;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->client = $client;
        $this->orderItems = $orderItems;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getOrderItems(): array
    {
        return $this->orderItems;
    }

    public function containsDeletedProducts(): bool
    {
        return !empty(array_filter($this->orderItems, function (OrderItem $orderItem) {
            return $orderItem->getProduct()->getDeleted();
        }));
    }
}