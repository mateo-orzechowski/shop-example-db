<?php

declare(strict_types=1);

namespace App\ViewModel\Order;

use App\ViewModel\Client\Client;

class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \string
     */
    private $totalPrice;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var bool
     */
    private $containsDeletedProducts;

    public function __construct(int $id, string $status, \DateTime $createdAt, ?string $totalPrice, Client $client, bool $containsDeletedProducts)
    {
        $this->id = $id;
        $this->status = $status;
        $this->createdAt = $createdAt;
        $this->totalPrice = $totalPrice;
        $this->client = $client;
        $this->containsDeletedProducts = $containsDeletedProducts;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getTotalPrice(): ?string
    {
        return $this->totalPrice;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function isContainsDeletedProducts(): bool
    {
        return $this->containsDeletedProducts;
    }
}