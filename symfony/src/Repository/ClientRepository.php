<?php

namespace App\Repository;

use App\Entity\Client;
use App\ViewModel\Client\Client as Model;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class ClientRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function count(array $filters)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from(Client::class, 'c')
        ;

        $this->addFilters($queryBuilder, $filters);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param array $filters
     * @param array $sortings
     * @param int $limit
     * @param int $page
     *
     * @return Model[]
     */
    public function findBy(array $filters, array $sortings, int $limit, int $page): array
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->select('c.id, c.name, c.surname, c.email')
            ->from(Client::class, 'c')
        ;

        $this->addFilters($queryBuilder, $filters);

        $query = $queryBuilder->getQuery();
        $query->setFirstResult(($page - 1) * $limit);
        $query->setMaxResults($limit);

        $data = $query->getResult();

        return array_map(function($data) {
            return new Model(
                $data['id'],
                $data['name'],
                $data['surname'],
                $data['email']
            );
        }, $data);
    }

    /**
     * @param int $id
     *
     * @return Model
     */
    public function find(int $id): Model
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->select('c.id as id, c.name, c.surname, c.email')
            ->from(Client::class, 'c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
        ;

        $data = $queryBuilder->getQuery()->getSingleResult();

        return new Model(
            $data['id'],
            $data['name'],
            $data['surname'],
            $data['email']
        );
    }

    private function addFilters(QueryBuilder $queryBuilder, array $filters): void
    {
        if (array_key_exists('search', $filters)) {
            $searchTokens = explode(' ', $filters['search']);

            $or = $queryBuilder->expr()->orX();
            $queryBuilder->andWhere($or);

            foreach ($searchTokens as $paramKey => $searchToken) {
                $or->add($queryBuilder->expr()->like('c.name', ":searchQuery_$paramKey"));
                $or->add($queryBuilder->expr()->like('c.surname', ":searchQuery_$paramKey"));
                $or->add($queryBuilder->expr()->like('c.email', ":searchQuery_$paramKey"));

                $queryBuilder->setParameter("searchQuery_$paramKey", "%{$searchToken}%");
            }

            $queryBuilder->andWhere($or);
        }
    }
}
