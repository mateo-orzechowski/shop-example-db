<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\ViewModel\Client\Client as ClientModel;
use App\ViewModel\Order\Order as OrderModel;
use App\ViewModel\Order\OrderShow;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

class OrderRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function count(array $filters): int
    {
        $queryBuilder =$this->entityManager->createQueryBuilder()
            ->select("DISTINCT o.id")
            ->from(Order::class, 'o')
            ->join('o.client', 'c')
            ->leftJoin('o.orderItems', 'oi')
            ->leftJoin('oi.product', 'p')
            ->groupBy('o.id');
        ;

        $this->addFilters($queryBuilder, $filters);

        $subQuery = $queryBuilder->getQuery()->getSQL();
        $parameters = array_map(function (Parameter $parameter) {
            return $parameter->getValue();
        }, $queryBuilder->getQuery()->getParameters()->toArray());

        $result = $this->entityManager
            ->getConnection()
            ->executeQuery("SELECT COUNT(*) FROM ($subQuery) as sub_query", $parameters)
            ->fetchAll();


        return array_values($result[0])[0];
    }


    public function find(int $id): ?OrderShow
    {
        $orderData = $this->entityManager->createQueryBuilder()
            ->select('
                o.id as order_id,
                o.status as order_status,
                o.createdAt as order_created_at,
                c.id as client_id,
                c.name as client_name,
                c.surname as client_surname,
                c.email as client_email
            ')
            ->from(Order::class, 'o')
            ->join('o.client', 'c')
            ->where('o.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (empty($orderData)) {
            return null;
        }

        $orderItemsData = $this->entityManager->getRepository(OrderItem::class)->findBy(['order' => $id]);

        return $this->hydrateShowOrder($orderData, $orderItemsData);
    }

    /**
     * @param string[] $filters
     * @param string[] $sorting
     * @param int $limit
     * @param int $page
     *
     * @return array
     */
    public function findBy(array $filters, array $sorting, int $limit, int $page): array
    {
        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('
                o.id as order_id,
                o.status as order_status,
                o.createdAt as order_created_at,
                SUM(p.price * oi.quantity) as order_total_price,
                c.id as client_id,
                c.name as client_name,
                c.surname as client_surname,
                c.email as client_email
            ')
            ->from(Order::class, 'o')
            ->join('o.client', 'c')
            ->leftJoin('o.orderItems', 'oi')
            ->leftJoin('oi.product', 'p')
            ->groupBy('o.id')
        ;

        $this->addSortings($queryBuilder, $sorting);
        $this->addFilters($queryBuilder, $filters);

        $query = $queryBuilder->getQuery();

        $query->setFirstResult(($page - 1) * $limit);
        $query->setMaxResults($limit);

        $ordersData = $query->getArrayResult();

        return $this->hydrateOrders($ordersData);
    }

    private function addSortings(QueryBuilder $queryBuilder, array $sorting): void
    {
        if (array_key_exists('createdAt', $sorting)) {
            $queryBuilder->addOrderBy('o.createdAt', $sorting['createdAt']);
        }

        if (array_key_exists('totalPrice', $sorting)) {
            $queryBuilder->addOrderBy('SUM(p.price * oi.quantity)', $sorting['totalPrice']);
        }
    }

    private function addFilters(QueryBuilder $queryBuilder, array $filters): void
    {
        if (array_key_exists('createdAt', $filters)) {
            if (array_key_exists('from', $filters['createdAt'])) {
                $queryBuilder
                    ->andWhere('o.createdAt >= :createdAt_from')
                    ->setParameter('createdAt_from', $filters['createdAt']['from']);

            }
            if (array_key_exists('to', $filters['createdAt'])) {
                $queryBuilder
                    ->andWhere('o.createdAt <= :createdAt_to')
                    ->setParameter('createdAt_to', $filters['createdAt']['to']);

            }
        }

        if (array_key_exists('client', $filters)) {
            $queryBuilder
                ->andWhere('o.client = :client')
                ->setParameter('client', $filters['client']);

        }

        if (array_key_exists('totalPrice', $filters)) {
            if (array_key_exists('from', $filters['totalPrice'])) {
                $queryBuilder
                    ->andHaving('SUM(p.price * oi.quantity) >= :totalPrice_from')
                    ->setParameter('totalPrice_from', (float) $filters['totalPrice']['from']);

            }
            if (array_key_exists('to', $filters['totalPrice'])) {
                $queryBuilder
                    ->andHaving('SUM(p.price * oi.quantity) <= :totalPrice_to')
                    ->setParameter('totalPrice_to', (float) $filters['totalPrice']['to']);

            }
        }

        if (array_key_exists('status', $filters)) {
            $queryBuilder
                ->andWhere('o.status = :status')
                ->setParameter('status', $filters['status']);
        }
    }

    private function hydrateOrders(array $ordersData): array
    {
        return array_map(function (array $orderData) {
            return $this->hydrateOrder($orderData);
        }, $ordersData);
    }

    private function hydrateOrder(array $orderData): OrderModel
    {
        $client = new ClientModel(
            $orderData['client_id'],
            $orderData['client_name'],
            $orderData['client_surname'],
            $orderData['client_email']
        );

        return new OrderModel(
            $orderData['order_id'],
            $orderData['order_status'],
            $orderData['order_created_at'],
            $orderData['order_total_price'],
            $client,
//            $orderData['order_contains_deleted_products']
             false
        );
    }

    private function hydrateShowOrder(array $orderData, array $orderItemsData): OrderShow
    {
        $client = new ClientModel(
            $orderData['client_id'],
            $orderData['client_name'],
            $orderData['client_surname'],
            $orderData['client_email']
        );

//        $orderItem = array_map(function (array $orderItemData) {
//            return new OrderItemModel(...);
//        }, $orderItemsData);

        $orderItems = $orderItemsData;

        return new OrderShow(
            $orderData['order_id'],
            $orderData['order_status'],
            $orderData['order_created_at'],
            $client,
            $orderItems
        );
    }
}
