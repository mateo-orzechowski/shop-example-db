<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

     /**
      * @return Product[] Returns an array of Product objects
      */
    public function findAllBy(array $filters, array $sortings, int $limit, int $page)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $this->addSortings($queryBuilder, $sortings);
        $this->addFilters($queryBuilder, $filters);

        $query = $queryBuilder->getQuery();

        $query->setFirstResult(($page - 1) * $limit);
        $query->setMaxResults($limit);

        return $query->getResult();
    }

    private function addSortings(QueryBuilder $queryBuilder, array $sorting): void
    {
        if (array_key_exists('name', $sorting)) {
            $queryBuilder->addOrderBy('p.name', $sorting['name']);
        }

        if (array_key_exists('price', $sorting)) {
            $queryBuilder->addOrderBy('p.price', $sorting['price']);
        }
    }

    private function addFilters(QueryBuilder $queryBuilder, array $filters): void
    {
        if (array_key_exists('price', $filters)) {
            if (array_key_exists('from', $filters['price'])) {
                $queryBuilder
                    ->andWhere('p.price >= :from')
                    ->setParameter('from', $filters['price']['from']);

            }
            if (array_key_exists('to', $filters['price'])) {
                $queryBuilder
                    ->andWhere('p.price <= :to')
                    ->setParameter('to', $filters['price']['to']);

            }
        }

        if (array_key_exists('name', $filters)) {
            $queryBuilder
                ->andWhere('p.name LIKE :nameQuery')
                ->setParameter('nameQuery', "{$filters['name']}%");
        }

        if (array_key_exists('deleted', $filters)) {
            $queryBuilder
                ->andWhere('p.deleted = :deleted')
                ->setParameter('deleted', $filters['deleted'] === 'false' ? false : $filters['deleted'] == 'true' ? true : false);
        }
    }
}
