<?php

declare(strict_types=1);

namespace App\Util;

class Pagination
{
    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $pages;

    /**
     * @var int
     */
    private $total;

    /**
     * @var int
     */
    private $page;

    /**
     * @var string
     */
    private $path;

    /**
     * @var Query
     */
    private $query;

    public function __construct(int $limit, int $total, int $page, string $path, Query $query = null)
    {
        $this->limit = $limit;
        $this->total = $total;
        $this->page = $page;

        $this->pages = (int) ceil($total / $limit);
        $this->path = $path;
        $this->query = $query;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getPages(): int
    {
        return $this->pages;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getQuery(): Query
    {
        return $this->query;
    }

    public function getPageUrl(int $page): string
    {
        $url = '{{ url }}?page={{ page }}&limit={{ limit }}';

        if ($this->query) {
            $params = $this->query->getQueryString();

            if (!empty($params) ) {
                $url .= "&$params";
            }
        }

        $url = str_replace('{{ url }}', $this->path, $url);
        $url = str_replace('{{ page }}', $page, $url);
        $url = str_replace('{{ limit }}', $this->limit, $url);

        return $url;
    }

    public function getPageUrls(): array
    {
        $urls = [];

        $urls['Prev'] = null;

        if ($this->page > 1) {
            $urls['Prev'] = $this->getPageUrl($this->page - 1);
        }

        for($i = 1; $i <= $this->pages; $i++) {
            $urls[$i] = $this->getPageUrl($i);
        }

        $urls['Next'] = null;

        if ($this->page < $this->pages) {
            $urls['Next'] = $this->getPageUrl($this->page + 1);
        }

        return $urls;
    }
}