<?php

namespace App\Util\Converter;

class DotStringToArrayLikeString
{
    /**
     * Converts string 'array.key_1.key_2' to 'array[key_1][key_2]'
     *
     * @param string $dotString
     *
     * @return string
     */
    public static function convert(string $dotString): string
    {
        $stringParts = explode('.', $dotString);

        $arrayBaseName = array_shift($stringParts);

        $stringParts = array_map(function (string $stringPart) { return "[$stringPart]"; }, $stringParts);

        return $arrayBaseName . implode('', $stringParts);
    }
}