<?php

namespace App\Util\Converter;

class ArrayLikeStringToDotString
{
    /**
     * Converts string 'array[key_1][key_2]' to 'array.key_1.key_2'
     *
     * @param string $arrayLikeString
     *
     * @return string
     */
    public static function convert(string $arrayLikeString): string
    {
        preg_match_all('/(\w+)/', $arrayLikeString, $matches);

        if (!isset($matches[1])) {
            return '';
        }

        return implode('.', $matches[1]);
    }
}