<?php

namespace App\Util\Converter;

class ArrayToDotString
{
    /**
     * Converts array ['key_1' => ['key_2' => 'value', 'key_3' => 'value']]
     * to ['key_1.key_2' => 'value', 'key_1.key_3' => 'value'].
     *
     * @param array $array
     * @param array $parentKeys
     *
     * @return array
     */
    public static function convert(array $array, array $parentKeys = []): array
    {
        $flattenedArray = [];

        foreach ($array as $key => $arrayItem) {
            $currentKeys = $parentKeys;
            $currentKeys[] = $key;

            if (is_array($arrayItem)) {
                $flattenedArray = array_merge($flattenedArray, self::convert($arrayItem, $currentKeys));

                continue;
            }

            $flattenedArray[implode('.', $currentKeys)] = &$arrayItem;
        }

        return $flattenedArray;
    }
}