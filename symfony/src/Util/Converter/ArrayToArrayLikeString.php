<?php

namespace App\Util\Converter;

class ArrayToArrayLikeString
{
    /**
     * Converts array ['key_1' => ['key_2' => 'value', 'key_3' => 'value']]
     * to ['baseArrayName[key_1][key_2]' => 'value', 'baseArrayName[key_1][key_3]' => 'value'].
     *
     * @param array $array
     * @param string $baseArrayName
     *
     * @return array
     */
    public static function convert(array $array, string $baseArrayName = 'array'): array
    {
        $flattenedArray = [];

        foreach ($array as $key => $arrayItem) {
            $newKey = $baseArrayName . "[$key]";

            if (is_array($arrayItem)) {
                $flattenedArray = array_merge($flattenedArray, self::convert($arrayItem, $newKey));

                continue;
            }

            $flattenedArray[$newKey] = $arrayItem;
        }

        return $flattenedArray;
    }
}