<?php

namespace App\Util\Converter;

class ArrayLikeStringToArray
{
    /**
     * Converts string 'array[key_1][key_2]' to array ['key_1' => ['key_2' => 'value']]'
     *
     * @param string $arrayLikeString
     * @param $value
     * @param bool $ignoreFirstKey
     *
     * @return array
     */
    public static function convert(string $arrayLikeString, $value, bool $ignoreFirstKey = true): array
    {
        preg_match_all('/(\w+)/', $arrayLikeString, $matches);

        if (!isset($matches[1])) {
            return [];
        }

        $stringParts = $matches[1];

        if ($ignoreFirstKey) {
            array_shift($stringParts);
        }

        $array = [];
        $tmp = &$array;

        foreach ($stringParts as $stringPart) {
            $tmp = &$tmp[$stringPart];
        }

        $tmp = $value;
        unset($tmp);

        return $array;
    }
}