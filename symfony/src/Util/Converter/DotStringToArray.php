<?php

namespace App\Util\Converter;

class DotStringToArray
{
    /**
     * Converts string 'array.key_1.key_2' to array ['key_1' => ['key_2' => 'value']]'
     *
     * @param string $dotString
     * @param mixed $value
     * @param bool $ignoreFirstKey
     *
     * @return array
     */
    public static function convert(string $dotString, $value, bool $ignoreFirstKey = true): array
    {
        $stringParts = explode('.', $dotString);

        if ($ignoreFirstKey) {
            array_shift($stringParts);
        }

        $array = [];
        $tmp = &$array;

        foreach ($stringParts as $stringPart) {
            $tmp = &$tmp[$stringPart];
        }

        $tmp = $value;
        unset($tmp);

        return $array;
    }
}