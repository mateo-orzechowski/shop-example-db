<?php

declare(strict_types=1);

namespace App\Util;

use App\Util\Converter\ArrayToArrayLikeString;
use App\Util\Converter\ArrayToDotString;
use App\Util\Converter\DotStringToArrayLikeString;
use Symfony\Component\HttpFoundation\Request;

class Query
{
    /**
     * @var string[] e.g. $sorting['key'] = 'asc'
     */
    private $sortings = [];

    /**
     * @var array
     */
    private $filters = [];

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $page;

    /**
     * @var callable[]
     */
    private $valueGetters;

    /**
     * @var array
     */
    private $cachedValues = [];

    /**
     * @param string[] $sorting
     * @param array $filters
     * @param int $limit
     * @param int $page
     */
    public function __construct(array $sorting, array $filters, int $limit, int $page)
    {
        $this->sortings = $sorting;
        $this->filters = $filters;
        $this->limit = $limit;
        $this->page = $page;
    }

    /**
     * @return string[]
     */
    public function getSortings(): array
    {
        return $this->sortings;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public static function fromRequest(Request $request): self
    {
        return new self(
            $request->query->get('sorting', []),
            $request->query->get('filter', []),
            (int) $request->query->get('limit', 10),
            (int) $request->query->get('page', 1)
        );
    }

    public function getQueryString(): string
    {
        $combined = [];
        $combined = array_merge($combined, ArrayToArrayLikeString::convert($this->filters, 'filter'));
        $combined = array_merge($combined, ArrayToArrayLikeString::convert($this->sortings, 'sorting'));

        $queryStringParts = [];

        foreach ($combined as $paramKey =>  $paramValue) {
            $queryStringParts[] = "$paramKey=$paramValue";
        }

        return implode('&', $queryStringParts);
    }
}