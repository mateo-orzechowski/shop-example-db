<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\NewOrderType;
use App\Form\OrderType;
use App\Repository\ClientRepository;
use App\Repository\OrderRepository;
use App\Service\Order\OrderQueryParamService;
use App\Util\Pagination;
use App\Util\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("", name="order_list", methods={"GET"})
     *
     * @param OrderRepository $orderRepository
     * @param ClientRepository $clientRepository
     * @param Request $request
     * @param RouterInterface $router
     *
     * @return Response
     */
    public function listAction(
        OrderRepository $orderRepository,
        ClientRepository $clientRepository,
        Request $request,
        RouterInterface $router
    ): Response {

        $query = Query::fromRequest($request);

        $pagination = new Pagination(
            $query->getLimit(),
            $orderRepository->count($query->getFilters()),
            $query->getPage(),
            $router->generate('order_list'),
            $query
        );

        $filteredClient = null;

        if (array_key_exists('client', $query->getFilters())) {
            $filteredClient = $clientRepository->find($query->getFilters()['client']);
        }

        return $this->render('order/index.html.twig', [
            'orders' => $orderRepository->findBy($query->getFilters(), $query->getSortings(), $query->getLimit(), $query->getPage()),
            'query' => $query,
            'pagination' => $pagination,
            'filter' => ['clinet' => $filteredClient],
        ]);
    }

    /**
     * @Route("/new", name="order_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $order = new Order(new \DateTime());
        $form = $this->createForm(NewOrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($order);
            $entityManager->flush();

            return $this->redirectToRoute('order_list');
        }

        return $this->render('order/new.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_show", methods={"GET"})
     */
    public function show(int $id, OrderRepository $orderRepository): Response
    {
        return $this->render('order/show.html.twig', [
            'order' => $orderRepository->find($id),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="order_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Order $order): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_list', [
                'id' => $order->getId(),
            ]);
        }

        return $this->render('order/edit.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete'.$order->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($order);
            $entityManager->flush();
        }

        return $this->redirectToRoute('order_list');
    }
}
