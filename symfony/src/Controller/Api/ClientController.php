<?php

namespace App\Controller\Api;

use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Util\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/client")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/{client}", name="api_client_get", methods={"GET"})
     */
    public function getProduct(Client $client, SerializerInterface $serializer): Response
    {
        $data = $serializer->serialize($client, 'json');

        return new Response($data);
    }
    /**
     * @Route("", name="api_client_get_list", methods={"GET"})
     */
    public function getProducts(ClientRepository $repository, SerializerInterface $serializer, Request $request): Response
    {
        $query = Query::fromRequest($request);

        $data = $serializer->serialize($repository->findBy($query->getFilters(), $query->getSortings(), $query->getLimit(), $query->getPage()), 'json');

        return new Response($data);
    }
}
