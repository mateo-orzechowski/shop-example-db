<?php

namespace App\Controller\Api;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Util\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/{product}", name="api_product_get", methods={"GET"})
     */
    public function getProduct(Product $product, SerializerInterface $serializer): Response
    {
        $data = $serializer->serialize($product, 'json');

        return new Response($data);
    }
    /**
     * @Route("", name="api_product_get_list", methods={"GET"})
     */
    public function getProducts(ProductRepository $repository, SerializerInterface $serializer, Request $request): Response
    {
        $query = Query::fromRequest($request);

        $data = $serializer->serialize($repository->findAllBy($query->getFilters(), $query->getSortings(), $query->getLimit(), $query->getPage()), 'json');

        return new Response($data);
    }
}
