<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FilterExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('prepare_query_params', [$this, 'prepareQueryParams']),
        ];
    }

    public function prepareQueryParams(string $paramKey, array $queryParams)
    {
        return $this->getFlatQueryParam($paramKey, $queryParams);;
    }

    public function getFlatQueryParam(string $currentKey, array $queryParams): array
    {
        $temp = [];

        foreach ($queryParams as $key => $queryParam) {
            $newKey = $currentKey . "[$key]";

            if (is_array($queryParam)) {
                $temp = array_merge($temp, $this->getFlatQueryParam($newKey, $queryParam));
                continue;
            }

            $temp[$newKey] = $queryParam;
        }

        return $temp;
    }
}