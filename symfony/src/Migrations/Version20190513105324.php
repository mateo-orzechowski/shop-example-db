<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190513105324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          price NUMERIC(11, 2) NOT NULL, 
          unit VARCHAR(255) DEFAULT NULL, 
          amount NUMERIC(8, 2) DEFAULT NULL, 
          deleted TINYINT(1) DEFAULT NULL, 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          surname VARCHAR(255) NOT NULL, 
          email VARCHAR(255) NOT NULL, 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE _order (
          id INT AUTO_INCREMENT NOT NULL, 
          client_id INT NOT NULL, 
          created_at DATETIME NOT NULL, 
          INDEX IDX_7F117F0419EB6921 (client_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_item (
          id INT AUTO_INCREMENT NOT NULL, 
          product_id INT NOT NULL, 
          _order INT NOT NULL, 
          quantity NUMERIC(8, 2) NOT NULL, 
          INDEX IDX_52EA1F094584665A (product_id), 
          INDEX IDX_52EA1F097F117F04 (_order), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          _order 
        ADD 
          CONSTRAINT FK_7F117F0419EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE 
          order_item 
        ADD 
          CONSTRAINT FK_52EA1F094584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE 
          order_item 
        ADD 
          CONSTRAINT FK_52EA1F097F117F04 FOREIGN KEY (_order) REFERENCES _order (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_item DROP FOREIGN KEY FK_52EA1F094584665A');
        $this->addSql('ALTER TABLE _order DROP FOREIGN KEY FK_7F117F0419EB6921');
        $this->addSql('ALTER TABLE order_item DROP FOREIGN KEY FK_52EA1F097F117F04');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE _order');
        $this->addSql('DROP TABLE order_item');
    }
}
