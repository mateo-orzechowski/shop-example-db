class Select {
    constructor($select) {
        this.$select = $select;

        this.options = {};
        this.selectedOption = {};

        this.init();
    }

    init() {
        if (this.$select.data('select')) {
            return;
        }

        this.$select.data('select', this);

        this.$optionsContainer = this.$select.find('.options-container');
        this.$autocompleteInput = this.$optionsContainer.find('.autocomplete-input');
        this.$selectedOptions = this.$select.find('.selected-options');
        this.$formInput = this.$select.find('.form-input');

        let handler = this.$select.data('handler').split('.');

        this.handler = {
            object: handler[0],
            method: handler[1]
        };

        this.selectedOption = {value: this.$formInput.val()};

        this.$selectedOptions.click(() => {
            this.$optionsContainer.toggleClass('hidden');
            window[this.handler.object][this.handler.method]().done((options) => {this.setOptions(options); this.showOptions(this.options);});
        });

        this.$autocompleteInput.keydown((e) => {
            if (e.originalEvent.code == 'Escape') {
                this.$optionsContainer.toggleClass('hidden');
            }
        });

        this.deleyied = null;

        this.$autocompleteInput.keyup((e) => {

            clearInterval(this.deleyied);

            this.deleyied = setTimeout(() => {
                let value = e.target.value;
                window[this.handler.object][this.handler.method](value).done((options) => {
                    this.setOptions(options);
                    this.showOptions(this.options);
                });
            }, 500);
        });
    }

    showOptions(options) {
        this.$optionsContainer.find('.options').html('');

        for (let key in options) {
            if (!options.hasOwnProperty(key) || key == this.selectedOption.value) continue;

            let $option = this.createOption(options[key]);
            this.$optionsContainer.find('.options').append($option);
        }
    }

    createOption(option) {
        let $option = $(`<div class="option" data-value="${option.value}">${option.label}</div>`);

        $option.click((e) => {
            this.select($(e.target), option.value);
        });

        return $option;
    }

    select($option, value) {
        if (this.options.hasOwnProperty(value)) {
            this.selectedOption = this.options[value];
            delete this.options[value];
            this.showSelected();
        }

        this.$optionsContainer.toggleClass('hidden');
    }

    showSelected() {
        this.$selectedOptions.find('p.selected-option').remove();
        this.$selectedOptions.append($(`<p class="selected-option">${this.selectedOption.label}</p>`));

        this.$formInput.val(this.selectedOption.value);
        this.$formInput.attr('value', this.selectedOption.value);
        this.$formInput.trigger('change');
    }

    setOptions(options) {

        this.options = {};

        options.forEach((elem, key) => {
            this.options[elem.value] = elem;
        })
    }

    disable() {
        this.$select.attr('disabled', 'disabled');
    }
}

$(document).on('click', '.form-collection-add-new-entry', function () {
    let $this = $(this);

    let collectionHolderId = $this.data('collection-holder');
    let $collectionHolder = $(`#${collectionHolderId}`);

    let counter = $collectionHolder.data('item-counter');

    let templateId = $collectionHolder.data('item-template');
    let $template = $(`#${templateId}`);
    let template = $template.html().trim();
    template = template.replace(/__name__/g, counter);

    let $newRow = $(template);
    $newRow.data('item-index', counter);

    $collectionHolder.data('item-counter', counter + 1);

    let btnId = $this.attr('id');
    let addNewRowHandler = window[btnId];

    addNewRowHandler($collectionHolder, $newRow);
});

$(document).ready(function () {
    $('.select').each(function (key, elem) {
        new Select($(elem));
    })
});
