const API_PRODUCT_GET = '/api/product/{product}';
const API_PRODUCT_LIST = '/api/product';

class Product {
    constructor(id, name, price, unit, amount, deleted) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.amount = amount;
        this.deleted = deleted;
    }
}

class ProductRepository {
    constructor(productMapper) {
        this.productMapper = productMapper;
    }

    getProduct(productId) {
        return $.get(API_PRODUCT_GET.parameters({'product': productId}))
            .then((res) => {
                return this.productMapper.map(JSON.parse(res))
            })
    }

    getAutocompleteOptions(queryString = '') {
        return $.get(API_PRODUCT_LIST.parameters({'filter[name]': queryString, 'filter[deleted]': false}))
            .then((res) => {
                return JSON.parse(res).map((elem) => {
                    return this.productMapper.map(elem);
                })
            })
            .then((products) => {
                return products.map((product) => {
                    return {value: product.id, label: product.name};
                })
            })
    }
}

class ProductMapper {
    map(productData) {
        return new Product(
            productData.id,
            productData.name,
            productData.price,
            productData.unit,
            productData.amount,
            productData.deleted
        );
    }
}
