const API_CLIENT_LIST = '/api/client';

class Client {
    constructor(id, name, surname, email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
    }
}

class ClientRepository {

    constructor(clientMapper) {
        this.clientMapper = clientMapper;
    }

    getAutocompleteOptions(queryString = '') {
        return $.get(API_CLIENT_LIST.parameters({
            'filter[search]': queryString,
        }))
            .then((res) => {
                return JSON.parse(res).map((elem) => {
                    return this.clientMapper.map(elem);
                })
            })
            .then((clients) => {
                return clients.map((client) => {
                    return {value: client.id, label: `${client.name} ${client.surname} (${client.email})`};
                })
            })
    }
}

class ClientMapper{
    map(clientData) {
        return new Client(
            clientData.id,
            clientData.name,
            clientData.surname,
            clientData.email,
        );
    }
}
