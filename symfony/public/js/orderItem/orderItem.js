class OrderItem {
    constructor(product = null, quantity = 0) {
        this.product = product;
        this.quantity = quantity;
    }

    setProduct(product) {
        this.product = product;
    }

    setQuantity(quantity) {
        this.quantity = quantity;
    }

    sum()
    {
        if (!this.product || !this.product.price) {
            return 0;
        }

        return this.product.price * this.quantity;
    }
}

class OrderItemService {
    constructor (filter) {
        this.orderItems = [];
        this.onAddHandlres = [];
        this.onRemoveHandlers = [];
        this.onEmptyHandlers = [];

        this.filter = filter;
    }

    add(orderItem) {
        if (!this.filter || !this.filter(orderItem)) {
            return;
        }

        if (this.orderItems.indexOf(orderItem) > -1) {
            return;
        }

        this.orderItems.push(orderItem);

        this.onAddHandlres.forEach((fun, index) => {
            fun(orderItem);
        });
    }

    onAdd(fun) {
        this.onAddHandlres.push(fun);
    }

    remove(orderItem) {
        let index = this.orderItems.indexOf(orderItem);

        if (index < 0) {
            return;
        }

        this.orderItems.splice(index, 1);

        this.onRemoveHandlers.forEach((fun) => {
            fun(orderItem);
        });

        if (this.orderItems.length < 1) {
            this.onEmptyHandlers.forEach((fun) => {
                fun(orderItem);
            });
        }
    }

    onRemove(fun) {
        this.onRemoveHandlers.push(fun);
    }

    onEmpty(fun) {
        this.onEmptyHandlers.push(fun);
    }
}

class OrderItemHtml {
    static get PRODUCT_INPUT_SELECTOR() {return 'input[id^="order_orderItems_"][id$="_product"]'};
    static get QUANTITY_INPUT_SELECTOR() {return 'input[id^="order_orderItems_"][id$="_quantity"]'};

    static get AMOUNT_CELL_SELECTOR() {return 'td._order_orderItems_orderItem_product_amount'};
    static get PRICE_CELL_SELECTOR() {return 'td._order_orderItems_orderItem_product_price'};
    static get SUM_CELL_SELECTOR() {return 'td._order_orderItems_orderItem_sum'};
    static get REMOVE_BUTTON_SELECTOR() {return '._order_orderItems_orderItem_delete_item'};

    constructor(productRepository, orderItemWithDeletedProductService, $row, order) {
        this.productRepository = productRepository;
        this.orderItemWithDeletedProductService = orderItemWithDeletedProductService;
        this.$row = $row;
        this.order = order;

        this.init();

        this.$productInput.change((e) => {
            let val = e.target.value;

            this.productRepository.getProduct(val).done((product) => {
                this.orderItem.product = product;
                this.update();

                if (product.deleted) {
                    this.$row.addClass('deleted-product-warning');
                    this.orderItemWithDeletedProductService.add(this.orderItem);
                } else {
                    this.$row.removeClass('deleted-product-warning');
                    this.orderItemWithDeletedProductService.remove(this.orderItem);
                }
            });
        });

        this.$quantityInput.keyup((e) => {
            this.orderItem.quantity = e.target.value;
            this.update();
        });

        this.$removeBtn.click(() => {
            this.$row.remove();
            this.orderItemWithDeletedProductService.remove(this.orderItem);
        });
    }

    update() {
        this.$amountCell.html(`${format(this.orderItem.product.amount ? this.orderItem.product.amount : 0, 2)} ${this.orderItem.product.unit ? this.orderItem.product.unit : ''}`);
        this.$priceCell.html(format(this.orderItem.product.price ? this.orderItem.product.price : 0, 2));
        this.$sumCell.html(format(this.orderItem.sum(), 2));
    }

    init() {
        this.orderItem = new OrderItem();

        this.$row.data('order-item-data', this);

        this.$removeBtn = this.$row.find(OrderItemHtml.REMOVE_BUTTON_SELECTOR);

        this.$productInput = this.$row.find(OrderItemHtml.PRODUCT_INPUT_SELECTOR);
        this.$quantityInput = this.$row.find(OrderItemHtml.QUANTITY_INPUT_SELECTOR);

        this.$amountCell = this.$row.find(OrderItemHtml.AMOUNT_CELL_SELECTOR);
        this.$priceCell = this.$row.find(OrderItemHtml.PRICE_CELL_SELECTOR);
        this.$sumCell = this.$row.find(OrderItemHtml.SUM_CELL_SELECTOR);

        this.orderItem.quantity = this.$quantityInput.val();

        this.$productInput.parents('td').first().find('.select')

        this.select = new Select(this.$productInput.parents('td').first().find('.select'));

        if (!this.order.isPending()) {
            console.log(this.order);

            this.select.disable();
            this.$quantityInput.attr('readonly', 'readonly');
            this.$quantityInput.addClass('disabled');
            this.$removeBtn.addClass('disabled');
        }

        this.productRepository.getProduct(this.$productInput.val()).done((product) => {
            this.orderItem.quantity = this.$quantityInput.val();
            this.orderItem.product = product;
            this.update();

            if (product.deleted && this.order.isPending()) {
                this.$row.addClass('deleted-product-warning');
            }

            this.orderItemWithDeletedProductService.add(this.orderItem);
        });
    }
}