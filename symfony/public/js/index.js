window.clientMapper = new ClientMapper();
window.clientRepository = new ClientRepository(clientMapper);

window.productMapper = new ProductMapper();
window.productRepository = new ProductRepository(productMapper);

window.orderItemService = new OrderItemService();
window.orderItemWithDeletedProductService = new OrderItemService(orderItem => orderItem.product.deleted);