class Order {
    constructor(id, status) {
        if (status == undefined) status = 'pending';

        this.id = id;
        this.status = status;
    }

    isPending() {
        return this.status == 'pending';
    }
}

class OrderHtml {

    constructor($orderForm, orderItemWithDeletedProductService) {
        this.$orderForm = $orderForm;
        this.$table = this.$orderForm.find('#order_orderItems');
        this.$entryAnchor = null;
        this.items = [];
        this.itemCounter = 0;

        this.orderItemWithDeletedProductService = orderItemWithDeletedProductService;

        this.init();

        let _this = this;

        this.$table.on('form-collection-entry-add', function (event, $newRow) {
            _this.handelNewEntry(_this, $newRow)
        });

        this.orderItemWithDeletedProductService.onEmpty(() => {
            $('h4.deleted-product-warning').hide();
            $('#order_save')
                .attr('disabled', false)
                .removeClass('disabled');
        });

        this.orderItemWithDeletedProductService.onAdd(() => {
            if (!this.order.isPending()) {
                return;
            }

            $('h4.deleted-product-warning').show();
            $('#order_save')
                .attr('disabled', true)
                .addClass('disabled');
        });
    }

    handelNewEntry(_this, $newEntry) {
        $newEntry.find('td._order_orderItems_orderItem_index').html(++this.itemCounter);

        _this.items.push(new OrderItemHtml(productRepository, orderItemWithDeletedProductService, $newEntry, _this.order));
        _this.$entryAnchor.append($newEntry);
    }

    init() {
        this.order = new Order(this.$orderForm.find('#order_id').val(), this.$orderForm.find('#order_status').val());

        this.$entryAnchor = this.$table.find('tbody');

        let _this = this;

        this.$entryAnchor.find('tr').each((index, elem) => {
            _this.handelNewEntry(_this, $(elem));
        });
    }
}