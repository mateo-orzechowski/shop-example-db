function format(number, position) {
    let tmp = number * (Math.pow(10,position));
    tmp = parseInt(tmp);
    return (tmp / (Math.pow(10,position))).toFixed(position);
}

String.prototype.parameters = function (params) {

    let _this = this;
    let query = [];

    Object.keys(params).forEach(function (key) {
        let paramKey = `{${key}}`;

        if (_this.includes(paramKey)) {
            _this = _this.replace(paramKey, params[key]);

            return
        }

        if (params[key].length < 1) {
            return;
        }

        query.push(`${key}=${params[key]}`);
    });

    if (query.length > 0) {
        return _this + '?' + query.join('&');
    }

    return _this;
};

$(document).ready(function () {
    $('th.sortable').click(function () {
        window.location = $(this).data('sort-url');
    });

    $('tr.filters').find('button.apply-filter').click(function () {
        let url = window.location;
        let $filters = $('tr.filters').find('.filter');
        let filters = [];

        $filters.each(function (index, elem) {
            let $elem = $(elem);

            if (!$elem.val()) {
                return true;
            }

            filters.push(buildFilter($elem.data('filter-key'), $elem.val()));
        });

        let filtersQuery = filters.join('&');
        let sortingMatch = url.href.match(/.*\?.*(sorting%5.+%5D=(desc|asc)).*/);

        let newUrl = `${location.protocol}//${location.host}${location.pathname}`;

        if (sortingMatch) {
            newUrl += '?' + sortingMatch[1];
        }

        if (sortingMatch && filtersQuery.length > 0) {
            filtersQuery = '&' + filtersQuery;
        } else if (filters.length > 0) {
            filtersQuery = '?' + filtersQuery;
        }

        window.location = newUrl + encodeURI(filtersQuery);
    });
});

function buildFilter(filterKey, value) {
    let filterKeyParts = filterKey.split('.');

    return `filter[${filterKeyParts.join('][')}]=${value}`;
}