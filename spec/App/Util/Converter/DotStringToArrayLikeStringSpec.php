<?php

namespace spec\App\Util\Converter;

use PhpSpec\ObjectBehavior;

class DotStringToArrayLikeStringSpec extends ObjectBehavior
{
    function it_converts_dot_string_to_array_like_string()
    {
        $dotLikeString = 'array.key_1.key_2';

        $arrayLikeString = $this::convert($dotLikeString);

        $arrayLikeString->shouldBeLike('array[key_1][key_2]');
    }
}
