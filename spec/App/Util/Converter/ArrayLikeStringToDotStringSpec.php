<?php

namespace spec\App\Util\Converter;

use PhpSpec\ObjectBehavior;

class ArrayLikeStringToDotStringSpec extends ObjectBehavior
{
    function it_converts_array_like_string_to_dot_string()
    {
        $arrayLikeString = 'array[key_1][key_2]';

        $dotLikeString = $this::convert($arrayLikeString);

        $dotLikeString->shouldBeLike('array.key_1.key_2');
    }
}
