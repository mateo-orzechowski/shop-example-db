<?php

namespace spec\App\Util\Converter;

use PhpSpec\ObjectBehavior;

class ArrayLikeStringToArraySpec extends ObjectBehavior
{
    function it_converts_dot_string_to_array_ignoring_first_key()
    {
        $dotLikeString = 'array[key_1][key_2]';
        $value = 'value';

        $arrayLikeString = $this::convert($dotLikeString, $value);

        $arrayLikeString->shouldBeLike(['key_1' => ['key_2' => 'value']]);
    }

    function it_converts_dot_string_to_array_including_first_key()
    {
        $dotLikeString = 'array[key_1][key_2]';
        $value = 'value';

        $arrayLikeString = $this::convert($dotLikeString, $value, false);

        $arrayLikeString->shouldBeLike(['array' => ['key_1' => ['key_2' => 'value']]]);
    }
}
