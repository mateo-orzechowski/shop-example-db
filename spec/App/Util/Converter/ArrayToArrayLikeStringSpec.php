<?php

namespace spec\App\Util\Converter;

use PhpSpec\ObjectBehavior;

class ArrayToArrayLikeStringSpec extends ObjectBehavior
{
    function it_converts_complex_array_to_flattened_array_for_base_array_name()
    {
        $array = [
            'key_1' => [
                'key_2' => 'value_1',
                'key_3' => 'value_2',
            ]
        ];

        $convertedArray = $this::convert($array, 'base_array_name');

        $convertedArray->shouldBeLike([
            'base_array_name[key_1][key_2]' => 'value_1',
            'base_array_name[key_1][key_3]' => 'value_2',
        ]);
    }

    function it_converts_complex_array_to_flattened_array_without_base_array_name()
    {
        $array = [
            'key_1' => [
                'key_2' => 'value_1',
                'key_3' => 'value_2',
            ]
        ];

        $convertedArray = $this::convert($array);

        $convertedArray->shouldBeLike([
            'array[key_1][key_2]' => 'value_1',
            'array[key_1][key_3]' => 'value_2',
        ]);
    }
}
