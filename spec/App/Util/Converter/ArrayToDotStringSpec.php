<?php

namespace spec\App\Util\Converter;

use PhpSpec\ObjectBehavior;

class ArrayToDotStringSpec extends ObjectBehavior
{
    function it_converts_complex_array_to_flattened_array()
    {
        $array = [
            'key_1' => [
                'key_2' => 'value_1',
                'key_3' => 'value_2',
            ]
        ];

        $convertedArray = $this::convert($array, ['array']);

        $convertedArray->shouldBeLike([
           'array.key_1.key_2' => 'value_1',
           'array.key_1.key_3' => 'value_2',
        ]);
    }
}
